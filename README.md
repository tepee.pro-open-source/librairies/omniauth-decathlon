# Omniauth::Decathlon

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/omniauth/decathlon`. To experiment with that code, run `bin/console` for an interactive prompt.


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'omniauth-decathlon', :git => "https://gitlab.com/tepee.pro-open-source/librairies/omniauth-decathlon.git", :tag => 'vx.x.x'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install omniauth-decathlon

## Usage

Here's an example for adding the middleware to a Rails app in `config/initializers/omniauth.rb`:

```ruby
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :decathlon, ENV['DECATHLON_CLIENT_ID'], ENV['DECATHLON_CLIENT_SECRET'],
                    client_options: {
                        site: "decathlon_authent_endpoint ",
                        authorize_url: "decathlon_authorize_url",
                        token_url: "decathlon_token_url"
                    }
end
```
## Configuration

You can configure several options, which you pass in to the `provider` method via a hash:

* `scope`: A comma-separated list of permissions you want to request from the user. See the [Decathlon OAuth 2.0 website](https://sites.google.com/oxylane.com/fedid/how-it-works/oauth-how-it-works-in-details?authuser=1) for a full list of available permissions. Caveats:
  * The `openid`, `email` and `profile` scopes are used by default. By defining your own `scope`, you override these defaults, but Decathlon requires at least one of `email` or `profile`, so make sure to add at least one of them to your scope!


* `redirect_uri`: Override the redirect_uri used by the gem.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/tepee.pro-open-source/librairies/omniauth-decathlon.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
