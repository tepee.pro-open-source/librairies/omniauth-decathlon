require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Decathlon < OmniAuth::Strategies::OAuth2
      include OmniAuth::Strategy
      DEFAULT_SCOPE = 'profile,email,openid'.freeze

      option :client_options,
             site: 'https://idpdecathlon.oxylane.com',
             authorize_url: '/as/authorization.oauth2',
             token_url: '/as/token.oauth2'

      option :name, 'decathlon'

      option :authorize_options, %i[scope redirect_uri]

      def authorize_params
        super.tap do |params|
          options[:authorize_options].each do |k|
            params[k] = request.params[k.to_s] unless [nil, ''].include?(request.params[k.to_s])
          end

          params[:scope] = get_scope(params)
          params[:access_type] = 'offline' if params[:access_type].nil?

          session['omniauth.state'] = params[:state] if params[:state]
        end
      end

      def request_phase
        super
      end

      info do
        raw_info.merge('token' => access_token.token)
      end

      uid { raw_info['sub'] }

      info do
        {
          'first_name' => raw_info['givenName'],
          'last_name' => raw_info['familyName'],
          'email' => raw_info['mail'],
          'spoken_languages' => raw_info['preferredLanguage'],
          'profile_picture' => raw_info['photourl'],
          'company_name' => options.name
        }
      end
      extra do
        {
          'site_name' => raw_info['sitename']
        }
      end

      def raw_info
        @raw_info ||= access_token.get('/idp/userinfo.openid', {:headers => { 'Accept-Encoding' => '' }}).parsed
      end

      def build_access_token
        verifier = request.params['code']
        options.token_params[:headers] = { 'Accept-Encoding' => '' }
        client.auth_code.get_token(verifier, { redirect_uri: callback_url }.merge(token_params.to_hash(symbolize_keys: true)), deep_symbolize(options.auth_token_params))
      end

      def get_scope(params)
        raw_scope = params[:scope] || DEFAULT_SCOPE
        scope_list = raw_scope.split(' ').map { |item| item.split(',') }.flatten
        scope_list.join(' ')
      end

      def basic_auth_header
        'Basic ' + Base64.strict_encode64("#{options[:client_id]}:#{options[:client_secret]}")
      end

      def callback_url
        options[:redirect_uri] || (full_host + script_name + callback_path)
      end
    end
  end
  end
