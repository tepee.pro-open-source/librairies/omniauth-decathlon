lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "omniauth/decathlon/version"

Gem::Specification.new do |spec|
  spec.add_runtime_dependency 'omniauth', '~> 1.9'
  spec.add_runtime_dependency 'omniauth-oauth2', '~> 1.6'

  spec.name          = "omniauth-decathlon"
  spec.version       = Omniauth::Decathlon::VERSION
  spec.authors       = ["Alexis Dufour"]
  spec.email         = ["alexisdu59@orange.fr"]

  spec.summary       = %(A Decathlon OAuth2 strategy for OmniAuth)
  spec.description   = %(A Decathlon OAuth2 strategy for OmniAuth For ruby app)
  spec.homepage      = "https://gitlab.com/tepee_couching_pro/omniauth-decathlon"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.executables   = `git ls-files -- bin/*`.split("\n").collect { |f| File.basename(f) }
  spec.require_paths = %w[lib]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
